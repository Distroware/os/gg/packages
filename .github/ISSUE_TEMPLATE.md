Note:

- We will delete issues not following this template!
- You can add new information besides the ones required here.
- You can delete the subsections that are not related to your issue.

## 👶 For requesting new packages

- Link to the package(s) in AUR: [put link here](https://example.com)

- Utility this package has for you:

  Describe it here, why you use this package, how it can be helpful.

- Do you consider this package(s) to be useful for **every** chaotic user?:

  - [ ] YES
  - [ ] No, but yes for a great amount.
  - [ ] No, but yes for a few.
  - [ ] No, it's useful only for me.

- Do you consider this package(s) to be useful for feature testing/preview (e.g: mesa-aco, wine-wayland)?:

  - [ ] YES
  - [ ] NO

- Are you sure we don't have this package already (test with `pacman -Ss <pkgname>`)?:

  - [ ] YES

- Have you tested if this package builds in a clean chroot?:

  - [ ] YES
  - [ ] NO

- Does the package's license allows us to redistribute it?:

  - [ ] YES
  - [ ] No clue.
  - [ ] No, but the author doesn't really care, it's just for bureaucracy.

- Have you searched the [issues](https://github.com/chaotic-aur/packages/issues) to ensure this request is new (not duplicated)?:

  - [ ] YES

- Have you read the [README](https://github.com/chaotic-aur/packages#banished-and-rejected-packages) to ensure this package is not banned?:

  - [ ] YES

## 👴 For reporting outdated packages

- (If available) Link to latest build ([find it here](https://builds.garudalinux.org/repos/chaotic-aur/logs/)): [some-package.log](https://builds.garudalinux.org/repos/chaotic-aur/logs/some-package.log)

- Package name: `some-package`

- Latest build: `v0.0.0.r0.gc0ffec0ffe`

- Latest version available: `v1.0.0.r100.gdeadbeef`

- Have you tested if the latest version of the package builds in a clean chroot?:

  - [ ] YES
  - [ ] NO

## 🐛 For reporting bugs

- What happens?

  Describe it here

- What is expected to happen?

  Describe it here

- If possible, please attach logs.

## For security reports.

- Depending on how critical is making this information available, please send it to [critical at chaotic dot cx](mailto:critical@chaotic.cx)

- Describe it briefly: Include the attacking vector, the possible outcomes and, if possible, solutions.

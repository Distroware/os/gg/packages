## Maintainers
# Firefox
nss-hg
nspr-hg
--
firefox-wayland-hg
--

# Pedro
servo-git
--

# Nico
libreoffice-slim-fixed-git
linux-cacule
linux-xanmod-anbox
firedragon-hg

--

## User requests
# Issue 331
linux-mainline

# Issue 318
linux-next-git

# Issue 394
icecat

# Issue 446
librewolf-hg

# Issue 616
brave

# Issue 640
rpcs3-git
wolfssl # (dep rpcs3-git)

# Issue 698
yuzu-git:https://gitlab.com/tallero/yuzu-git-aur.git

# Issue 708
emacs-ng
emacs-ng-git

# Issue 729
linux-vfio

# Issue 774
freecad-git

# Issue 792
linux-g14
linux-zen-g14

# Issue 793
dogecoin-qt

# Issue 808
codelite-unstable

# Issue 833
lean4-git

# Issue 920
aura
aura-git

# Issue 923
amdvlk-2021q2.5
lib32-amdvlk-2021q2.5

# Issue 927
openblas-lapack


# -------------------------------- MOVED FROM KITSUNA
# Pedro
#darling-git (broken)
llvm-tkg-git:https://github.com/Frogging-Family/llvm-git.git

# Solomon
python-sphinx-automodapi
llvm-git:https://github.com/chaotic-aur/llvm-git.git
lib32-llvm-git:https://github.com/chaotic-aur/lib32-llvm-git.git
flat-remix
flat-remix-gnome
lmdbxx
tweeny
ayatana-indicator-datetime
nvm
python-pympv
python-grpcio-tools
calligra-git
kwaterfoxhelper
waterfox-classic-kpe
vim-colorschemes


# Moved because too heavy for Garuda
ungoogled-chromium

# Daily builds are enough
code-git

# Users requests
#wine-git # Issue 315 (bad)
scummvm-git # Issue 316
